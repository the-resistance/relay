# relay

A GUNDB relay server.

## Deployment

### Docker-Compose

To deploy the Docker image, you may use the included docker-compose.yml configuration:

1. docker-compose up -d
2. Your service will be available at: http://localhost:9669/gun